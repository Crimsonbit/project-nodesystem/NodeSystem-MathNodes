package at.crimsonbit.nodesystem.node.math;

import java.math.BigDecimal;

import at.crimsonbit.nodesystem.nodebackend.module.NodeModule;
import at.crimsonbit.nodesystem.nodebackend.module.NodeRegistry;

public class MathNodeModule extends NodeModule {

	@Override
	public void registerNodes(NodeRegistry registry) {
		registry.registerDefaultFactory(MathNodes.ADD, AddNode.class);
		registry.registerDefaultFactory(MathNodes.DIVIDE, DivideNode.class);
		registry.registerDefaultFactory(MathNodes.REMAINDER, RemainderNode.class);
		registry.registerDefaultFactory(MathNodes.MULTIPLY, MultiplyNode.class);
		registry.registerDefaultFactory(MathNodes.SUBTRACT, SubtractNode.class);

		registry.registerCast(Number.class, BigDecimal.class, n -> BigDecimal.valueOf(n.longValue()));
		registry.registerCast(Double.class, BigDecimal.class, n -> BigDecimal.valueOf(n));
		registry.registerCast(Float.class, BigDecimal.class, n -> BigDecimal.valueOf(n));

		registry.registerSerialization(BigDecimal.class, d -> d.toString(), s -> new BigDecimal(s));
	}
}
