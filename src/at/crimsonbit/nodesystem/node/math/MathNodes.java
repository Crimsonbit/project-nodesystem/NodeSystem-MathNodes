package at.crimsonbit.nodesystem.node.math;

import at.crimsonbit.nodesystem.nodebackend.INodeType;

/**
 *
 * @author Florian Wagner
 *
 */
public enum MathNodes implements INodeType {
	ADD("Addition"), SUBTRACT("Subtraction"), MULTIPLY("Multiply"), DIVIDE("Divide"), REMAINDER("Remainder");

	private String name;

	private MathNodes(String s) {

		this.name = s;
	}

	@Override
	public String nodeName() {
		return this.name;
	}

}
