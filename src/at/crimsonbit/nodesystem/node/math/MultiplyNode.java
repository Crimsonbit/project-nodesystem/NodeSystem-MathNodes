package at.crimsonbit.nodesystem.node.math;

import java.math.BigDecimal;

import at.crimsonbit.nodesystem.nodebackend.annotations.NodeField;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeInput;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeOutput;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;

public class MultiplyNode extends AbstractNode {

	@NodeInput
	@NodeField
	BigDecimal a= BigDecimal.ZERO;

	@NodeInput
	@NodeField
	BigDecimal b= BigDecimal.ZERO;

	@NodeOutput("compute")
	BigDecimal output;

	public MultiplyNode() {

	}

	@Override
	public void compute() {
		output = a.multiply(b);
	}

}
