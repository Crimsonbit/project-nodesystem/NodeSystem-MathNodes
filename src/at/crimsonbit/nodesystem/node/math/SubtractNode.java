package at.crimsonbit.nodesystem.node.math;

import java.math.BigDecimal;

import at.crimsonbit.nodesystem.nodebackend.annotations.NodeField;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeInput;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeOutput;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;

public class SubtractNode extends AbstractNode {

	@NodeInput
	@NodeField
	BigDecimal minuend = BigDecimal.ZERO;

	@NodeInput
	@NodeField
	BigDecimal subtrahend = BigDecimal.ZERO;

	@NodeOutput("compute")
	BigDecimal output;

	public SubtractNode() {

	}

	@Override
	public void compute() {
		output = minuend.subtract(subtrahend);
	}

}
